class FilesController < ApplicationController
  before_action :authenticate_user!

  def index
    @files = current_user.user_files
  end

  def upload
    user_file = UserFile.new(upload_params)
    user_file.user_id = current_user.id
    if user_file.save
      redirect_to root_url, notice: "File has been successfully uploaded!"
    else
      redirect_to root_url, alert: "Failed to upload File!"
    end
  end

  def destroy
    @my_file = UserFile.find(params[:id])
    # Only the user who created the file can destroy it.
    if @my_file.user_id == current_user.id
      @my_file.destroy
      redirect_to root_url
    else
      redirect_to root_url, alert: "Not Authorized!"
    end
  end

  private
  def upload_params
    params.permit(:name, :attachment)
  end
end
