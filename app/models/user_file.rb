class UserFile < ActiveRecord::Base
  belongs_to :user
  mount_uploader :attachment, AttachmentUploader
  validates :name, presence: true
  before_destroy :remove_file_from_disk

  private

  def remove_file_from_disk
    File.delete("#{Rails.root}#{self.attachment.url}")
  end
end
